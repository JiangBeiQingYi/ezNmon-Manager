﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>编辑用户</title>
	<link rel="stylesheet" href="${request.contextPath}/css/bootstrap.min.css">
	<script src="${request.contextPath}/js/jquery/jquery.min.js"></script>
	<script src="${request.contextPath}/js/jquery.validation/1.14.0/jquery.validate.min.js"></script>
	<script src="${request.contextPath}/js/jquery.validation/1.14.0/messages_zh.min.js"></script>
	<style>
		input.error{
			border: 1px solid #E6594E;
		}
	</style>
</head>
<body>
    <div class="container">
    <form id="editHostForm">
		  <br/>
		  <div class="form-group">
			<label for="hostname">主机名称：</label>
			<input type="text" class="form-control" id="hostname" name="hostname"  value="${data.name}" placeholder="输入主机名称">
		  </div>
		  <div class="form-group">
			<label for="hostaddr">主机IP：</label>
			${data.addr}<input type="hidden" id="hostaddr" name="hostaddr"  value="${data.addr}">
		  </div>
		   <div class="form-group">
			<label for="hostaddr">SSH端口号：</label>
			<input type="text" class="form-control" id="hostport" name="hostport" value="${data.port}" placeholder="输入SSH端口号">
		  </div>
		  <div class="form-group">
			<label for="hostusername">主机用户名：</label>
			<input type="text" class="form-control" id="hostusername" name="hostusername"  value="${data.username}" placeholder="主机用户名">
		  </div>
		  <div class="form-group">
			<label for="hostpassword">主机密码：</label>
			<input type="password" class="form-control" id="hostpassword" name="hostpassword"  value="${data.password}" placeholder="主机密码">
		  </div>
		   <div class="form-group">
				<label for="eznmonport">监控端口号：</label> <input type="text"
					class="form-control" id="eznmonport" name="eznmonport" value="${data.eznmonport}"
					placeholder="输入EasyNmon端口号">
			</div>
		  <div class="form-group">
				<label>系统类型：（默认nmon支持CentOS6~7等，Ubuntu和SUSE等版本需要选择）</label> <select id="ostype" name="ostype"  class="form-control">
					<option value="">--请选择--</option>
				</select>
				<input type="hidden" id="hostype" name="hostype"  value="${data.ostype}" >
			</div>
		   <div class="form-group">
			<label for="hosteremark">主机说明：</label>
			<input type="text" class="form-control" id="hostremark" name="hostremark"  value="${data.remark}" placeholder="主机说明">
		  </div>
		  <div class="form-group">
		  <button type="button" id="saveBtn" class="btn btn-success">提交</button>
		  <button type="button" id="cancelBtn" class="btn btn-default">取消</button>
		  </div>
		</form>
    </div>

		<script>
		function getContextPath(){
		    var pathName = document.location.pathname;
		    var index = pathName.substr(1).indexOf("/");
		    var result = pathName.substr(0,index+1);
		    return result;
		}
		var contextPath = getContextPath();
			var editHost = function(){
				if(!check().form()){ 
					return;  
				}
				$.ajax({
					   type: "GET",
					   dataType: "json",
					   url: contextPath+"/host/edit",
					   data: {
					        "id": ${data.id},
							"name": $("#hostname").val(),
							"addr":$("#hostaddr").val(),
							"port":$("#hostport").val(),
							"username":$("#hostusername").val(),
							"password":$("#hostpassword").val(),
							"remark":$("#hostremark").val(),
							"eznmonport":$("#eznmonport").val(),
							"ostype":$("#ostype").val()
					   },
					   success: function(msg){
							$('#cancelBtn').click();			
					   }
				});
			}
			
			$('#saveBtn').on('click', function(){
				editHost();
			});
			
			$('#cancelBtn').on('click', function(){
				var index = parent.layer.getFrameIndex(window.name);
				parent.getHostPageList();
				parent.layer.close(index);
			});

			
			//校验字段是否正确 
           function check(){ 
                /*返回一个validate对象，这个对象有一个form方法，返回的是是否通过验证*/ 
                return $("#editHostForm").validate({ 
                            rules:{ 
                                hostname:{ 
                                    required:true
                                },
                                hostaddr:{ 
                                    required:true                                   
                                },
                                hostport:{ 
                                    required:true                                   
                                },
                                hostusername:{ 
                                    required:true                                   
                                },
                                hostpassword:{ 
                                    required:true                                   
                                }
                            }, 
                            messages:{ 
                                hostname:{ 
                                    required:""
                                },
                                hostaddr:{ 
                                    required:""                                
                                },
                                hostport:{ 
                                    required:""                                
                                },
                                hostusername:{ 
                                    required:""                                
                                },
                                hostpassword:{ 
                                    required:""                                
                                }
                            }     
                        }); 
            }
            
            function getOSList() {
			$.ajax({
				type : "GET",
				dataType : "json",
				url : contextPath + "/easynmon/getostype",
				data : {},
				success : function(msg) {
					$('#ostype').html('');
					var options = '';
					options += '<option value="">--请选择--</option>';
					var pair = msg.message.split(",");
					for (var i = 0; i < pair.length; i++) {
						options += '<option value="' + pair[i] + '">' + pair[i]
								+ '</option>';
					}
					$('#ostype').append(options);
					//alert(document.getElementById('hostype').value);
					document.getElementById('ostype').value=document.getElementById('hostype').value;
				}
			});
		}
		 getOSList();
		
		</script> 	
</body>
</html>