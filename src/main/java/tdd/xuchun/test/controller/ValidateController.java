package tdd.xuchun.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tdd.xuchun.test.model.Host;
import tdd.xuchun.test.model.Result;
import tdd.xuchun.test.service.IHostService;

/**
 * 主机控制器
 * 
 * @author ZSL
 *
 */
@RestController
@RequestMapping("validate")
public class ValidateController{
	
	/**
	 * 注入主机业务服务
	 */
	@Autowired
	private IHostService hostService;

	
	/**
	 * 查询单个主机详细信息
	 * 
	 * @return
	 */
	@RequestMapping("/checkip")
	public Result checkIP(Host host){
		Host rsHost = hostService.getHostByIP(host);
		Result rs=new Result();
		if (rsHost!=null) {
			rs.setMessage("false");
		}
		return rs;
	}
	
}
