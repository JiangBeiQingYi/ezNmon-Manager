package tdd.xuchun.test.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

public class TarUntil {
	/*
	 * 返回包中的文件名
	 */
	public static List<String> visitTARGZ(File targzFile) throws IOException {
		List<String> filenamelist = new ArrayList<String>();
		FileInputStream fileIn = null;
		BufferedInputStream bufIn = null;
		GZIPInputStream gzipIn = null;
		TarArchiveInputStream taris = null;
		try {
			fileIn = new FileInputStream(targzFile);
			bufIn = new BufferedInputStream(fileIn);
			gzipIn = new GZIPInputStream(bufIn);
			taris = new TarArchiveInputStream(gzipIn);
			TarArchiveEntry entry = null;
			while ((entry = taris.getNextTarEntry()) != null) {
				if (entry.isDirectory())
					continue;
				filenamelist.add(entry.getName());
			}
		} finally {
			taris.close();
			gzipIn.close();
			bufIn.close();
			fileIn.close();
		}
		return filenamelist;
	}
}
