package tdd.xuchun.test.dao;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import tdd.xuchun.test.model.Host;

/**
 * 主机DAO数据接口
 * 
 * @author ZSL
 *
 */
@Repository
public interface HostMapper {
	
	/**
	 * 查询单个主机明细
	 * 
	 * @param host
	 * @return
	 */
	public Host getHost(Host host);
	
	/**
	 * 查询单个主机明细利用IP
	 * 
	 * @param host
	 * @return
	 */
	public Host getHostByIP(Host host);
	
	/**
	 * 增加新主机
	 * 
	 * @param host
	 */
	public void addHost(Host host);
	/**
	 * 删除主机
	 * 
	 * @param host
	 */
	public void delHost(Host host);
	/**
	 * 修改主机
	 * 
	 * @param host
	 */
	public void editHost(Host host);
	/**
	 * 模糊查询匹配的主机列表
	 * 
	 * @param host
	 * @return
	 */
	public List<Object> getLikeHosts(Map<String,Object> host);
	/**
	 * 模糊查询匹配的主机的数量
	 * 
	 * @param host
	 * @return
	 */
	public Integer getLikeHostsCount(Map<String,Object> host);

}
